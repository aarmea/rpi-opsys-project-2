#!/usr/bin/env python
'''
memsim.py
CSCI-4210 Project 2
Anthony DeRossi (derosa3@rpi.edu) and Albert Armea (armeaa@rpi.edu)
'''

import sys

MEM_SIZE = 2400
LINE_WIDTH = 80

def cmp1st(x, y):
  """Compare the first elements of two lists or tuples."""
  return cmp(x[0], y[0])

def cmp2nd(x, y):
  """Compare the second elements of two lists or tuples."""
  return cmp(x[1], y[1])

class AllocError(Exception):
  """An error encountered while allocating memory"""

def allocNoncontiguous(memory, pid, n, lastBlock=0):
  """An allocation algorithm that allocates the first 'n' free blocks in memory
  for the given process"""
  addr = 0
  addrs = []

  # Identify available blocks
  while len(addrs) < n and addr < len(memory):
    if memory[addr] == ".":
      addrs.append(addr)

    addr += 1

  # Raise an exception if there aren't enough blocks
  if len(addrs) < n:
    raise AllocError();

  # Allocate blocks
  for addr in addrs:
    memory[addr] = pid

  return addrs[-1]

def findChunks(memory, pid="."):
  """Find chunks of memory with the given pid and return them as a list of
  tuples of the form (start, size)"""
  start = -1
  size = 0
  chunks = []
  for block in xrange(len(memory)):
    if memory[block] == pid and start < 0:
      # Start of a chunk with pid
      start = block
      size = 1
    elif memory[block] == pid and start >= 0:
      # Continuing a chunk with pid
      size += 1
    elif (memory[block] != pid) and start >= 0:
      # End of a chunk with pid, or end of memory: add to list of chunks
      chunks.append((start, size))
      start = -1
      size = 0

  # If a chunk is still remaining at the end of memory
  if start > 0:
    chunks.append((start, size))

  return chunks

def allocRange(memory, pid, addr, size):
  """Allocate the given pid to the chunk in memory"""
  for index in xrange(size):
    block = addr + index
    memory[block] = pid

def allocFirst(memory, pid, n, lastBlock=0):
  """An allocation algorithm that allocates the first contiguous chunk of free
  memory large enough to hold the given process to that process"""
  return allocNext(memory, pid, n, 0)

def allocBest(memory, pid, n, lastBlock=0):
  """An allocation algorithm that allocates space in the smallest available
  chunk that has enough space."""
  # Find all chunks that are large enough
  chunks = filter(lambda chunk: chunk[1] >= n, findChunks(memory))

  # Fail if there are no chunks
  if not len(chunks):
    raise AllocError()

  # Sort the chunks by size
  chunks.sort(cmp2nd)

  # Allocate blocks within the smallest chunk
  addr = chunks[0][0]
  allocRange(memory, pid, addr, n)

  return addr + n

def allocNext(memory, pid, n, lastBlock=0):
  """An allocation algorithm that allocates the first contiguous chunk of free
  memory since lastBlock large enough to hold the given process to that 
  process"""
  freeChunks = findChunks(memory)
  chunks = filter(lambda chunk: chunk[1] >= n, freeChunks)
  nextChunks = filter(lambda chunk: lastBlock+n <= chunk[0]+chunk[1], chunks)

  if (len(nextChunks) > 0):
    chunks = nextChunks

  # Raise an exception if there isn't a large enough chunk
  if len(chunks) == 0:
    raise AllocError()

  # Allocate blocks
  if chunks[0][0] <= lastBlock and lastBlock+n <= chunks[0][0]+chunks[0][1]:
    addr = lastBlock
  else:
    addr = chunks[0][0]
  allocRange(memory, pid, addr, n)

  return addr + n

def allocWorst(memory, pid, n, lastBlock=0):
  """An allocation algorithm that allocates space in the largest available
  chunk."""
  # Find the largest available chunk
  chunks = findChunks(memory)
  chunks.sort(cmp2nd, reverse=True)

  # Fail if there are no chunks
  if not len(chunks):
    raise AllocError()

  # Get the target chunk
  addr, size = chunks[0]

  # Fail if the largest chunk is not large enough
  if n > size:
    raise AllocError()

  # Allocate blocks within the chunk
  allocRange(memory, pid, addr, n)

  return addr + n

def free(memory, pid):
  """Mark all memory with pid as free"""
  for address in xrange(len(memory)):
    if memory[address] == pid:
      memory[address] = "."

def compact(memory):
  """Return a compacted array of memory, maximizing the number of free
  blocks."""
  usedMemory = filter(lambda x: x != ".", memory)
  freeMemory = filter(lambda x: x == ".", memory)
  return usedMemory + freeMemory

def usage(pname):
  """Print the correct usage of the program."""
  print "USAGE:", pname, "<input-file> { noncontig | first | best | next | worst }"

def load(filename):
  """Load the processes from a file, returning lists of enter and exit times."""
  with open(filename, 'rb') as processes:
    # A list of (time, pid, size)
    enters = []
    # A list of (time, pid)
    exits = []

    for line in processes:
      args = line.split(' ')

      # Skip invalid lines
      if len(args) < 4:
        continue

      # Get the pid and size
      pid = args[0]
      size = int(args[1])

      # Get the enter and exit times
      times = args[2:]
      for i, time in enumerate(times):
        # Even indices are enter times
        if i & 1:
          exits.append((int(time), pid))
        else:
          enters.append((int(time), pid, size))

  enters.sort(cmp1st)
  exits.sort(cmp1st)

  return enters, exits

def wrap(s, cols):
  """Return a string containing the contents of 's' wrapped to the given number
  of columns."""
  rows = zip(*[s[col::cols] for col in xrange(cols)])
  return "\n".join("".join(row) for row in rows)

def dump(memory):
  """Print the contents of the memory array to screen."""
  print wrap(memory, LINE_WIDTH)

def main():
  """Run the simulation"""
  # Check the arguments
  if len(sys.argv) < 3:
    usage(sys.argv[0])
    return 1

  algos = {
    "noncontig": allocNoncontiguous,
    "first": allocFirst,
    "best": allocBest,
    "next": allocNext,
    "worst": allocWorst,
  }

  # Select the allocation algorithm
  try:
    alloc = algos[sys.argv[2]]
  except KeyError:
    print "Not a valid algorithm:", sys.argv[2]
    return 1

  # Load the process file
  enters, exits = load(sys.argv[1])

  # Initialize the simulation
  memory = ["."] * MEM_SIZE
  t = 0
  tNext = 0
  lastBlock = 0

  # Allocate memory for the operating system
  allocNoncontiguous(memory, "#", 80)

  # Run until there is nothing left to do
  while len(enters) or len(exits):
    # Handle exiting processes
    while len(exits) and exits[0][0] == t:
      # Remove the process
      _, pid = exits.pop(0)

      # Free memory
      free(memory, pid)

    # Handle entering processes
    while len(enters) and enters[0][0] == t:
      # Remove the process
      _, pid, size = enters.pop(0)

      # Allocate memory
      try:
        lastBlock = alloc(memory, pid, size, lastBlock)
      except AllocError:
        print
        print "OUT-OF-MEMORY"

        if alloc != allocNoncontiguous:
          # Get the kinds of processes out there
          pids = set(memory)

          # Get the process layout
          layoutBefore = dict((pid, findChunks(memory, pid)) for pid in pids)

          # Attempt to make room for the process by compacting
          print
          print "Performing defragmentation..."
          memory = compact(memory)
          print "Defragmentation completed."

          # Get the size of the free memory chunk
          chunks = findChunks(memory)
          try:
            _, freeSize = chunks[0]
          except IndexError:
            freeSize = 0

          # Get the process layout
          layoutAfter = dict((pid, findChunks(memory, pid)) for pid in pids)

          # Get the number of changed processes
          changes = 0
          for k, v in layoutBefore.iteritems():
            if layoutAfter[k] != v:
              changes += 1

          print ("Relocated %d processes to create a free memory block of %d"
            " units (%.2f%% of total memory)." %
            (changes, freeSize, float(freeSize) / len(memory) * 100))
          print

          # Try again
          try:
            lastBlock = alloc(memory, pid, size, lastBlock)
          except AllocError:
            print "OUT-OF-MEMORY"
            return 1

    # Handle user interaction
    if t >= tNext:
      print "Memory at time %d:" % t
      dump(memory)
      print

      # Prompt the user for the next time
      while True:
        tNextStr = raw_input("Enter a destination time or 0 to quit: ")
        try:
          tNext = int(tNextStr)
          break
        except ValueError:
          print "Enter a number"

      if tNext == 0:
        break

      print

    else:
      t += 1

if __name__ == "__main__":
  main()
